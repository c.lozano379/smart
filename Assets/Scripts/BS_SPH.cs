using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Particle
{

    public GameObject particleObject;
    public float roh;
    public List<Particle> Neighbours;
    public bool hasCalculatedNeighbours = false;

    public Vector3 positionThisFrame;
    public Vector3 velocityThisFrame;
    public float mass;
    public Vector3 forceToApply;
}


public class BS_SPH_Thread
{
    public List<Particle> particles;
    public bool jobDone;
    public bool goNextStep;
    public bool appRuning;    
    public Thread job;

    public BS_SPH_Thread()
    {
        jobDone = false;
        goNextStep = false;
        this.job = null;
        appRuning = true;
    }
}

public class BS_SPH : MonoBehaviour
{
    public Vector3Int numberOfCubesAlongAxes;
    public float d; // d is max interraction distance between particles 
    public float k;
    public float roh0;
    public float mu;
    public float maxForce;
    public int nbThreads;
    public bool showGrid;
    public WaterGenerator waterGenerator;

    private List<Particle>[,,] particleGrid;
    private Vector3 thisPosition;
    private List<BS_SPH_Thread> BS_SPH_threads;
    private List<Particle> particles;

    private Vector3 gridOrigin
    {
        get
        {
            return thisPosition - ((Vector3)numberOfCubesAlongAxes * d) / 2.0f;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        thisPosition = this.transform.position;

        particleGrid = new List<Particle>[numberOfCubesAlongAxes.x, numberOfCubesAlongAxes.y, numberOfCubesAlongAxes.z];
        for (int x = 0; x < numberOfCubesAlongAxes.x; x++)
        {
            for (int y = 0; y < numberOfCubesAlongAxes.y; y++)
            {
                for (int z = 0; z < numberOfCubesAlongAxes.z; z++)
                {
                    particleGrid[x, y, z] = null;
                }
            }
        }

        this.particles = waterGenerator.generateParticles();
        initParticleMass();

        BS_SPH_threads = new List<BS_SPH_Thread>();
        for (int i = 0; i < nbThreads; i++)
        {
            BS_SPH_Thread newThread = new BS_SPH_Thread();
            Thread t = new Thread(() => asyncMethods(newThread));
            newThread.job = t;
            newThread.job.Start();

            newThread.particles = particles.GetRange(i * (particles.Count / nbThreads), particles.Count / nbThreads);

            BS_SPH_threads.Add(newThread);
        }

    }

    private void OnDestroy()
    {
        for (int i = 0; i < nbThreads; i++)
        {
            BS_SPH_threads[i].job.Abort();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       

        updateParticleDataThisFrame();
        float timeAtBeginning = Time.realtimeSinceStartup;

        storeCellPositions();
        //Debug.Log("Store Cells Positions :" + (Time.realtimeSinceStartup - timeAtBeginning));
        timeAtBeginning = Time.realtimeSinceStartup;

        for (int i = 0; i < nbThreads; i++)
        {
            BS_SPH_threads[i].jobDone = false;
            BS_SPH_threads[i].goNextStep = true;
        }

        // UpdateNeighbours();

        for (int i = 0; i < nbThreads; i++)
        {
            while (!BS_SPH_threads[i].jobDone) { }
        }

        //Debug.Log("Update Neighbours :" + (Time.realtimeSinceStartup - timeAtBeginning));
        timeAtBeginning = Time.realtimeSinceStartup;


        for (int i = 0; i < nbThreads; i++)
        {
            BS_SPH_threads[i].jobDone = false;
            BS_SPH_threads[i].goNextStep = true;
        }

        // updateRoh();

        for (int i = 0; i < nbThreads; i++)
        {
            while (!BS_SPH_threads[i].jobDone) { }
        }

        
        //Debug.Log("Update Roh" + (Time.realtimeSinceStartup - timeAtBeginning));
        timeAtBeginning = Time.realtimeSinceStartup;

        for (int i = 0; i < nbThreads; i++)
        {
            BS_SPH_threads[i].jobDone = false;
            BS_SPH_threads[i].goNextStep = true;
        }

        // updateForces();

        for (int i = 0; i < nbThreads; i++)
        {
            while (!BS_SPH_threads[i].jobDone) { }
        }

        applyForces();

        //Debug.Log("forces" + (Time.realtimeSinceStartup - timeAtBeginning));
        timeAtBeginning = Time.realtimeSinceStartup;

        resetGrid();
    }

    private void asyncMethods(BS_SPH_Thread thread)
    {
        while (thread.appRuning)
        {
            while (!thread.goNextStep)
            { }

            thread.goNextStep = false;
            updateNeighboursAsync(thread.particles);
            thread.jobDone = true;


            while (!thread.goNextStep)
            { }

            thread.goNextStep = false;
            updateRohAsync(thread.particles);
            thread.jobDone = true;

            while (!thread.goNextStep)
            { }

            thread.goNextStep = false;
            calculateForceAsync(thread.particles);
            thread.jobDone = true;

        }
    }

    private void applyForces()
    {
        for(int i = 0; i < particles.Count; i++)
        {
            particles[i].particleObject.GetComponent<Rigidbody>().AddForce(particles[i].forceToApply * Time.deltaTime);
        }            
    }

    private void calculateForceAsync(List<Particle> particles)
    {
        for (int i = 0; i < particles.Count; i++)
        {
            Vector3 force = superNavierStokes(particles[i]);
            if (force.magnitude < maxForce)
                particles[i].forceToApply = force;
            else
                particles[i].forceToApply = Vector3.zero;

        }
    }

    private void updateRohAsync(List<Particle> particles)
    {
        for (int i = 0; i < particles.Count; i++)
        {
            particles[i].roh = roh(particles[i]);
        }
    }

    private void resetGrid()
    {
        for (int i = 0; i < particles.Count; i++)
        {
            Vector3Int particleCoords = getParticleCoordinatesInGrid(particles[i]);
            if(particleCoords.x != -1)
                particleGrid[particleCoords.x, particleCoords.y, particleCoords.z] = null;
        }

    }

    private void storeCellPositions()
    {
        for (int i = 0; i < particles.Count; i++)
        {
            Vector3Int particleCoordinates = getParticleCoordinatesInGrid(particles[i]);
            if (particleCoordinates.x == -1)
                continue;

            if (particleGrid[particleCoordinates.x, particleCoordinates.y, particleCoordinates.z] == null)
            {
                particleGrid[particleCoordinates.x, particleCoordinates.y, particleCoordinates.z] = new List<Particle>();
            }
            particleGrid[particleCoordinates.x, particleCoordinates.y, particleCoordinates.z].Add(particles[i]);
        }
    }

    private void updateParticleDataThisFrame()
    {
        for (int i = 0; i < particles.Count; i++)
        {
            particles[i].velocityThisFrame = particles[i].particleObject.GetComponent<Rigidbody>().velocity;
            particles[i].positionThisFrame = particles[i].particleObject.transform.position;
        }
    }

    private void initParticleMass()
    {
        for (int i = 0; i < particles.Count; i++)
        {
            particles[i].mass = particles[i].particleObject.GetComponent<Rigidbody>().mass;
        }
    }

    private Vector3Int getParticleCoordinatesInGrid(Particle p)
    {
        Vector3Int particleCoordinates = new Vector3Int(
                                                 Mathf.FloorToInt((p.positionThisFrame.x - gridOrigin.x) / d),
                                                 Mathf.FloorToInt((p.positionThisFrame.y - gridOrigin.y) / d),
                                                 Mathf.FloorToInt((p.positionThisFrame.z - gridOrigin.z) / d));

        if (particleCoordinates.x >= 0 && particleCoordinates.x < numberOfCubesAlongAxes.x
        && particleCoordinates.y >= 0 && particleCoordinates.y < numberOfCubesAlongAxes.y
        && particleCoordinates.z >= 0 && particleCoordinates.z < numberOfCubesAlongAxes.z)
        {
            return particleCoordinates;
        }
        else
        {
            Debug.LogWarning("Particle outside of grid !");
            return new Vector3Int(-1, -1, -1);
        }
    }

    private Vector3 superNavierStokes(Particle x)
    {
        return f_pressure_i(x) + f_viscosity_i(x);
    }

    private float WPoly6(float r) // r is destance between particles     15.5
    {
        float tmp = 0;
        if (r >= 0 && r <= d)
            tmp = Mathf.Pow(Mathf.Pow(d, 2) - Mathf.Pow(r, 2), 3);
        return (315 * tmp) / (64 * Mathf.PI * Mathf.Pow(d, 9));
    }

    private float gradWPoly6(float r)   // 15.9
    {
        float tmp = 0;
        if (r >= 0 && r <= d)
            tmp = (-6 * r * Mathf.Pow(Mathf.Pow(d, 2) - Mathf.Pow(r, 2), 2));

        return (315 * tmp) / (64 * Mathf.PI * Mathf.Pow(d, 9));
    }

    private float grad2WPoly6(float r)   // 15.9
    {
        float tmp = 0;
        if (r >= 0 && r <= d)
            tmp = 24 * Mathf.Pow(r, 2) * (Mathf.Pow(d, 2) - Mathf.Pow(r, 2)) - 6 * Mathf.Pow(Mathf.Pow(d, 2) - Mathf.Pow(r, 2), 2);
        return (315 * tmp) / (64 * Mathf.PI * Mathf.Pow(d, 9));
    }

    private float roh(Particle x)  //15.6
    {
        List<Particle> neighbours = getNeighbours(x);
        float sum = 0;
        for (int i = 0; i < neighbours.Count; i++)
        {
            sum += neighbours[i].mass * WPoly6((x.positionThisFrame - neighbours[i].positionThisFrame).magnitude);
        }
        return sum;
    }

    private List<Particle> getNeighbours(Particle x)
    {
        return x.Neighbours;

        /*List<Particle> toReturn = new List<Particle>();
        List<List<Particle>> neighbourCells = getAdjacentCells(getParticleCoordinatesInGrid(x));
        for(int i = 0; i < neighbourCells.Count; i++)
        {
            for(int j = 0; j < neighbourCells[i].Count; j++)
            {
                if(neighbourCells[i][j] != x)
                {
                    toReturn.Add(neighbourCells[i][j]);
                }
            }
        }
        return toReturn;
        */
    }

    private List<List<Particle>> getAdjacentCells(Vector3Int centerCell)
    {
        List<List<Particle>> toReturn = new List<List<Particle>>();

        for (int x = -1; x <= 1; x++)
        {
            if (centerCell.x + x < 0 || centerCell.x + x >= numberOfCubesAlongAxes.x)
                continue;
            for (int y = -1; y <= 1; y++)
            {
                if (centerCell.y + y < 0 || centerCell.y + y >= numberOfCubesAlongAxes.y)
                    continue;
                for (int z = -1; z <= 1; z++)
                {
                    if (centerCell.z + z < 0 || centerCell.z + z >= numberOfCubesAlongAxes.z)
                        continue;

                    if (particleGrid[centerCell.x + x, centerCell.y + y, centerCell.z + z] != null)
                    {
                        toReturn.Add(particleGrid[centerCell.x + x, centerCell.y + y, centerCell.z + z]);
                    }
                }
            }

        }

        return toReturn;
    }

    //private float A_s(Particle x)   //15.8
    //{
    //    List<Particle> neighbours = getNeighbours(x);
    //    float sum = 0;
    //    for (int i = 0; i < neighbours.Count; i++)
    //    {
    //        sum += neighbours[i].mass * (A_j / roh(neighbours[i])) * WPoly6((x.particleObject.transform.position - neighbours[i].particleObject.transform.position).magnitude); //todo
    //    }
    //    return sum;
    //}

    //private float gradA_s(Particle x)  //15.9
    //{
    //    List<Particle> neighbours = getNeighbours(x);
    //    float sum = 0;
    //    for (int i = 0; i < neighbours.Count; i++)
    //    {
    //        sum += neighbours[i].mass * (A_j / roh(neighbours[i])) * gradWPoly6((x.particleObject.transform.position - neighbours[i].particleObject.transform.position).magnitude); //todo
    //    }
    //    return sum;
    //}

    private Vector3 f_pressure_i(Particle x) //15.14
    {

        List<Particle> neighbours = getNeighbours(x);
        Vector3 sum = Vector3.zero;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if ((x.positionThisFrame - neighbours[i].positionThisFrame).magnitude < d)
            {
                sum += neighbours[i].mass
                * (p(x) + p(neighbours[i])) / (2 * neighbours[i].roh)
                * gradWPoly6((x.positionThisFrame - neighbours[i].positionThisFrame).magnitude)
                * (x.positionThisFrame - neighbours[i].positionThisFrame).normalized;
            }
        }
        return -sum;
    }

    private float p(Particle x) // 15.15
    {
        return k * (x.roh - roh0);
    }

    private Vector3 f_viscosity_i(Particle x) //15.17
    {
        List<Particle> neighbours = getNeighbours(x);
        Vector3 sum = Vector3.zero;
        for (int i = 0; i < neighbours.Count; i++)
        {
            if ((x.positionThisFrame - neighbours[i].positionThisFrame).magnitude < d)
            {
                sum += neighbours[i].mass
                    * (neighbours[i].velocityThisFrame - x.velocityThisFrame)
                    / (neighbours[i].roh)
                    * grad2WPoly6((x.positionThisFrame - neighbours[i].positionThisFrame).magnitude);
            }
        }
        return mu * sum;
    }

    private void updateNeighboursAsync(List<Particle> particles)
    {
        for (int i = 0; i < particles.Count; i++)
        {
            Vector3Int currentCell = getParticleCoordinatesInGrid(particles[i]);
            List<List<Particle>> adjacentCells = getAdjacentCells(currentCell);
            particles[i].Neighbours = new List<Particle>();

            for (int j = 0; j < adjacentCells.Count; j++)
            {
                for (int k = 0; k < adjacentCells[j].Count; k++)
                {
                    if (particles[i] != adjacentCells[j][k])
                    {
                        particles[i].Neighbours.Add(adjacentCells[j][k]);
                    }
                }
            }
        }
    }

    private void UpdateNeighbours()
    {
        //List<Thread> threads = new List<Thread>();

        //for(int i = 0; i < nbThreads; i++)
        //{
        //    List<Particle> rangeToProcess = particles.GetRange(i * (particles.Count / nbThreads), particles.Count / nbThreads);
        //    Thread t = new Thread(() => updateNeighboursAsync(rangeToProcess));
        //    threads.Add(t);
        //    t.Start();
        //}

        //for(int i = 0; i < threads.Count; i++)
        //{
        //    threads[i].Join();
        //}

    }

    private void OnDrawGizmos()
    {
        if(showGrid)
            displayGridGizmos();
    }

    private void displayGridGizmos()
    {
        if(d == 0)
        {
            Debug.LogError("D should be above 0");
            return;
        }
        Gizmos.color = Color.red;

        if (numberOfCubesAlongAxes.x <= 0 || numberOfCubesAlongAxes.y <= 0 || numberOfCubesAlongAxes.z <= 0)
        {
            Debug.LogError("ERROR : Grid resolution must be positive !");
            return;
        }



        for (float x = 0.0f; x <= numberOfCubesAlongAxes.x * d; x += d)
        {
            for (float y = 0.0f; y <= numberOfCubesAlongAxes.y * d; y += d)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(x, y, 0);
                Vector3 currentLineDestination = gridOrigin + new Vector3(x, y, numberOfCubesAlongAxes.z * d);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }

        for (float x = 0.0f; x <= numberOfCubesAlongAxes.x * d; x += d)
        {
            for (float z = 0.0f; z <= numberOfCubesAlongAxes.z * d; z += d)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(x, 0, z);
                Vector3 currentLineDestination = gridOrigin + new Vector3(x, numberOfCubesAlongAxes.y * d, z);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }

        for (float y = 0.0f; y <= numberOfCubesAlongAxes.y * d; y += d)
        {
            for (float z = 0.0f; z <= numberOfCubesAlongAxes.z * d; z += d)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(0, y, z);
                Vector3 currentLineDestination = gridOrigin + new Vector3(numberOfCubesAlongAxes.x * d, y, z);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }

    }
}

