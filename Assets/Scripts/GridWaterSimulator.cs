using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell
{
    public int nbParticles;
    public static Vector3 gridCellSize;
    public static Vector3 gridOrigin;
    public Vector3 totalVelocity;
    public Vector3 averageVelocity {
        get
        {
            if (nbParticles == 0)
            {
                return Vector3.zero;
            }
            return totalVelocity / nbParticles;
        }
    }
    public Vector3 totalPosition;
    public Vector3 centerPosition {
        get
        {
            if (nbParticles == 0)
            {
                return gridOrigin + new Vector3(gridCoordinates.x * gridCellSize.x + gridCellSize.x/2.0f,
                                   gridCoordinates.y * gridCellSize.y + gridCellSize.y / 2.0f,
                                   gridCoordinates.z * gridCellSize.z + gridCellSize.z / 2.0f);
            }
            return totalPosition / nbParticles;
        }
    } 
    public Vector3 pressureForce;
    public Vector3Int gridCoordinates;

    public GridCell(Vector3Int gridCoordinates)
    {
        nbParticles = 0;
        totalVelocity = Vector3.zero;
        pressureForce = Vector3.zero;
        totalPosition = Vector3.zero;
        this.gridCoordinates = gridCoordinates;
    }
}

public class GridWaterSimulator : MonoBehaviour
{
    public Vector3 gridSize;
    public Vector3Int numberOfCubesAlongAxes;
    public Vector3 gridCubeSize { get { return new Vector3(gridSize.x / numberOfCubesAlongAxes.x, gridSize.y / numberOfCubesAlongAxes.y, gridSize.z / numberOfCubesAlongAxes.z); } }
    public int numberOfParticles;
    public GameObject patriclePrefab;
    public float pressureForceFactor;
    public float averageVelocityForceFactor;
    public float yForce;

    private List<GameObject> particleObjects;
    private GridCell[,,] grid;
    Vector3 gridOrigin { get { return this.transform.position + new Vector3(-gridSize.x / 2.0f, -gridSize.y / 2.0f, -gridSize.z / 2.0f); } }

    // Start is called before the first frame update
    void Start()
    {
        GridCell.gridCellSize = gridCubeSize;
        GridCell.gridOrigin = gridOrigin;
        particleObjects = new List<GameObject>();
        grid = new GridCell[numberOfCubesAlongAxes.x, numberOfCubesAlongAxes.y, numberOfCubesAlongAxes.z];
        generateParticles();
    }

    // Update is called once per frame
    void Update()
    {

        clearGrid();

        for (int i = 0; i < particleObjects.Count; i++)
        {
            Vector3Int currentParticleCoordinates = getArrayIndexFromWorldCoordinates(particleObjects[i].transform.position);
            if (currentParticleCoordinates.x != -1)
            {
                ref GridCell currentCell = ref grid[currentParticleCoordinates.x, currentParticleCoordinates.y, currentParticleCoordinates.z];
                if (currentCell == null)
                    currentCell = new GridCell(currentParticleCoordinates);
                currentCell.nbParticles++;
                currentCell.totalVelocity += particleObjects[i].GetComponent<Rigidbody>().velocity;
                currentCell.totalPosition += particleObjects[i].transform.position;
            }
        }

        applyDensityForces();

        for(int i = 0; i < particleObjects.Count; i++)
        {
            Vector3Int currentParticleCoordinates = getArrayIndexFromWorldCoordinates(particleObjects[i].transform.position);
            if (currentParticleCoordinates.x != -1)
            {
                ref GridCell currentCell = ref grid[currentParticleCoordinates.x, currentParticleCoordinates.y, currentParticleCoordinates.z];
                particleObjects[i].GetComponent<Rigidbody>().AddForce(currentCell.averageVelocity * averageVelocityForceFactor);
                particleObjects[i].GetComponent<Rigidbody>().AddForce(currentCell.pressureForce);
                particleObjects[i].GetComponent<Rigidbody>().AddForce(new Vector3(0,yForce,0));
            }
        }
    }

    private void applyDensityForces()
    {
        for (int x = 0; x < numberOfCubesAlongAxes.x; x++)
        {
            for (int y = 0; y < numberOfCubesAlongAxes.y; y++)
            {
                for (int z = 0; z < numberOfCubesAlongAxes.z; z++)
                {
                    if(grid[x,y,z] != null)
                    {
                        GridCell[] adjacentCells = getAdjacentCells(new Vector3Int(x,y,z));
                        for(int i = 0; i < 6; i++)
                        {
                            if (adjacentCells[i] != null && grid[x, y, z].nbParticles != 0)
                            {
                                Vector3 direction = (adjacentCells[i].centerPosition - grid[x, y, z].centerPosition).normalized;
                                float force = (grid[x, y, z].nbParticles - adjacentCells[i].nbParticles)/((float)numberOfParticles / (float)(numberOfCubesAlongAxes.x * numberOfCubesAlongAxes.y * numberOfCubesAlongAxes.z));
                                grid[x, y, z].pressureForce += direction * force * pressureForceFactor;
                            }
                        }
                    }
                }
            }
        }
    }

    private GridCell[] getAdjacentCells(Vector3Int centerCell)
    {
        GridCell[] toReturn = new GridCell[6];

        Vector3Int[] cellsToConsider = new Vector3Int[6];
        cellsToConsider[0] = (centerCell + new Vector3Int(1, 0, 0));
        cellsToConsider[1] = (centerCell + new Vector3Int(-1, 0, 0));
        cellsToConsider[2] = (centerCell + new Vector3Int(0, 1, 0));
        cellsToConsider[3] = (centerCell + new Vector3Int(0, -1, 0));
        cellsToConsider[4] = (centerCell + new Vector3Int(0, 0, 1));
        cellsToConsider[5] = (centerCell + new Vector3Int(0, 0, -1));

        for (int i = 0; i < 6; i++)
        {
            if (cellsToConsider[i].x < 0 || cellsToConsider[i].x >= numberOfCubesAlongAxes.x
             || cellsToConsider[i].y < 0 || cellsToConsider[i].y >= numberOfCubesAlongAxes.y
             || cellsToConsider[i].z < 0 || cellsToConsider[i].z >= numberOfCubesAlongAxes.z)
            {
                toReturn[i] = null;
            }
            else if (grid[cellsToConsider[i].x, cellsToConsider[i].y, cellsToConsider[i].z] == null)
            {
                toReturn[i] = (new GridCell(cellsToConsider[i]));
            }
            else
            {
                toReturn[i] = (grid[cellsToConsider[i].x, cellsToConsider[i].y, cellsToConsider[i].z]);
            }
        }

        return toReturn;
    }

    private void generateParticles()
    {
        for (int i = 0; i < numberOfParticles; i++)
        {
            GameObject currentWaterParticle = GameObject.Instantiate(patriclePrefab);
            Vector3 particleOffset = new Vector3(Random.Range(0, gridSize.x), Random.Range(0, gridSize.y), Random.Range(0, gridSize.z));
            currentWaterParticle.transform.position = gridOrigin + particleOffset;
            currentWaterParticle.transform.SetParent(this.transform);
            particleObjects.Add(currentWaterParticle);
        }
    }

    private void OnDrawGizmos()
    {
        if(numberOfCubesAlongAxes.x <= 0 || numberOfCubesAlongAxes.y <= 0 || numberOfCubesAlongAxes.z <= 0)
        {
            Debug.LogError("ERROR : Grid resolution must be positive !");
            return;
        }

        

        for (float x = 0.0f; x <= gridSize.x; x += gridCubeSize.x)
        {
            for (float y = 0.0f; y <= gridSize.y; y += gridCubeSize.y)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(x, y, 0);
                Vector3 currentLineDestination = gridOrigin + new Vector3(x, y, gridSize.z);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }

        for (float x = 0.0f; x <= gridSize.x; x += gridCubeSize.x)
        {
            for (float z = 0.0f; z <= gridSize.z; z += gridCubeSize.z)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(x, 0, z);
                Vector3 currentLineDestination = gridOrigin + new Vector3(x, gridSize.y, z);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }

        for (float y = 0.0f; y <= gridSize.y; y += gridCubeSize.y)
        {
            for (float z = 0.0f; z <= gridSize.z; z += gridCubeSize.z)
            {
                Vector3 currentLineOrigin = gridOrigin + new Vector3(0, y, z);
                Vector3 currentLineDestination = gridOrigin + new Vector3(gridSize.x, y, z);
                Gizmos.DrawLine(currentLineOrigin, currentLineDestination);
            }
        }
    }

    private Vector3Int getArrayIndexFromWorldCoordinates(Vector3 worldCoordinates)
    {
        Vector3 coordinatesRelativeToGrid = worldCoordinates - gridOrigin;
        Vector3Int toReturn = new Vector3Int(Mathf.FloorToInt(coordinatesRelativeToGrid.x / gridCubeSize.x), Mathf.FloorToInt(coordinatesRelativeToGrid.y / gridCubeSize.y), Mathf.FloorToInt(coordinatesRelativeToGrid.z / gridCubeSize.z));

        if(toReturn.x < 0 || toReturn.x >= numberOfCubesAlongAxes.x
        || toReturn.y < 0 || toReturn.y >= numberOfCubesAlongAxes.y
        || toReturn.z < 0 || toReturn.z >= numberOfCubesAlongAxes.z)
        {
            return new Vector3Int(-1, -1, -1);
        }

        return toReturn;
    }

    private void clearGrid()
    {
        for(int x = 0; x < numberOfCubesAlongAxes.x; x++)
        {
            for (int y = 0; y < numberOfCubesAlongAxes.y; y++)
            {
                for (int z = 0; z < numberOfCubesAlongAxes.z; z++)
                {
                    grid[x, y, z] = null;
                }
            }
        }
    }
}
