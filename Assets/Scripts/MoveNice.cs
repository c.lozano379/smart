using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveNice : MonoBehaviour
{
    public Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Z))
            this.GetComponent<Rigidbody>().MovePosition(this.transform.position + (moveDirection * Time.deltaTime));
        if (Input.GetKey(KeyCode.S))
            this.GetComponent<Rigidbody>().MovePosition(this.transform.position - (moveDirection * Time.deltaTime));
    }
}
