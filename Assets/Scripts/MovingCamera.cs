using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCamera : MonoBehaviour
{
    private float currentAngle = 0;
    public Vector2 offsetToTarget;
    public GameObject target;
    public float cameraSpeed;
    // Start is called before the first frame update
    void Start()
    {
        currentAngle = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Q))
            currentAngle -= cameraSpeed;
        if (Input.GetKey(KeyCode.D))
            currentAngle += cameraSpeed;

        this.transform.position = target.transform.position + new Vector3(Mathf.Cos(currentAngle / 360 * 2 * Mathf.PI) * offsetToTarget.x, offsetToTarget.y, Mathf.Sin(currentAngle / 360 * 2 * Mathf.PI) * offsetToTarget.x);
        this.transform.LookAt(target.transform);
    }
}
