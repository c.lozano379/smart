using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : MonoBehaviour
{
    public Vector3 dimCube;
    public int nbWaterParticles;
    public GameObject waterParticlePrefab;
    private Vector3 cubeOrigin { get { return new Vector3(-dimCube.x / 2.0f, -dimCube.y / 2.0f, -dimCube.z / 2.0f)+ this.transform.position; } }


    // Start is called before the first frame update
    void Start()
    {

    }

    public List<Particle> generateParticles()
    {
        List<Particle> particles = new List<Particle>();
        for (int i = 0; i < nbWaterParticles; i++)
        {
            Particle toAdd = new Particle();
            GameObject newParticle = GameObject.Instantiate(waterParticlePrefab);
            newParticle.transform.position = cubeOrigin + new Vector3(Random.Range(0, dimCube.x), Random.Range(0, dimCube.y), Random.Range(0, dimCube.z));
            newParticle.transform.SetParent(this.transform);
            toAdd.particleObject = newParticle;
            particles.Add(toAdd);
        }

        return particles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        // 4 points du bas
        Vector3 point1 = this.transform.position +
            new Vector3(-dimCube.x / 2.0f, -dimCube.y / 2.0f, -dimCube.z / 2.0f);
        Vector3 point2 = this.transform.position +
            new Vector3(dimCube.x / 2.0f, -dimCube.y / 2.0f, -dimCube.z / 2.0f);
        Vector3 point3 = this.transform.position +
            new Vector3(dimCube.x / 2.0f, -dimCube.y / 2.0f, dimCube.z / 2.0f);
        Vector3 point4 = this.transform.position +
            new Vector3(-dimCube.x / 2.0f, -dimCube.y / 2.0f, dimCube.z / 2.0f);

        // 4 points du haut
        Vector3 point5 = this.transform.position +
            new Vector3(-dimCube.x / 2.0f, dimCube.y / 2.0f, -dimCube.z / 2.0f);
        Vector3 point6 = this.transform.position +
            new Vector3(dimCube.x / 2.0f, dimCube.y / 2.0f, -dimCube.z / 2.0f);
        Vector3 point7 = this.transform.position +
            new Vector3(dimCube.x / 2.0f, dimCube.y / 2.0f, dimCube.z / 2.0f);
        Vector3 point8 = this.transform.position +
            new Vector3(-dimCube.x / 2.0f, dimCube.y / 2.0f, dimCube.z / 2.0f);

        // relier
        Gizmos.DrawLine(point1, point2);
        Gizmos.DrawLine(point2, point3);
        Gizmos.DrawLine(point3, point4);
        Gizmos.DrawLine(point4, point1);
        Gizmos.DrawLine(point1, point5);
        Gizmos.DrawLine(point2, point6);
        Gizmos.DrawLine(point3, point7);
        Gizmos.DrawLine(point4, point8);
        Gizmos.DrawLine(point5, point6);
        Gizmos.DrawLine(point6, point7);
        Gizmos.DrawLine(point7, point8);
        Gizmos.DrawLine(point8, point5);

    }
}
