using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterParticle : MonoBehaviour
{
    public Dictionary<int, GameObject> collisionnedObjects;
    private int particleIndex;
    public float repelForce;
    public static int particleCounter = 0;
    public AnimationCurve forceOverDistance;
    public AnimationCurve forceOverVelocity;
    public float maxSpeed;
    // Start is called before the first frame update
    void Start()
    {
        this.particleIndex = particleCounter;
        particleCounter++;
        collisionnedObjects = new Dictionary<int, GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        foreach(KeyValuePair<int, GameObject> pair in collisionnedObjects)
        {
            Vector3 vecForce = (pair.Value.transform.position - this.transform.position);
            //pair.Value.GetComponent<Rigidbody>().AddForce((pair.Value.transform.position - this.transform.position).normalized 
            // /((pair.Value.transform.position - this.transform.position).magnitude) * repelForce);
            pair.Value.GetComponent<Rigidbody>().AddForce((vecForce.normalized * (forceOverDistance.Evaluate(vecForce.magnitude)+ 
                forceOverVelocity.Evaluate(this.GetComponent<Rigidbody>().velocity.magnitude/maxSpeed))/2.0f)*repelForce);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("wet")  && !collisionnedObjects.ContainsKey(other.gameObject.GetComponent<WaterParticle>().particleIndex))
        {
            collisionnedObjects.Add(other.gameObject.GetComponent<WaterParticle>().particleIndex, other.gameObject);
            //Debug.Log(other.gameObject.name);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("wet") && collisionnedObjects.ContainsKey(other.gameObject.GetComponent<WaterParticle>().particleIndex))
        {
            collisionnedObjects.Remove(other.gameObject.GetComponent<WaterParticle>().particleIndex);
            //Debug.Log(other.gameObject.name);
        }
    }
}
