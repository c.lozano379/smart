Projet SMART  

Pour l'installation, simplement recupérer le contenu du git et ouvrir le projet avec unity :
 - lancer unity (version 2020.3.5f1)
 - ouvrir le projet
 - plusieurs scènes sont disponibles (Assets/Scenes/*)

l'échelle de temps peut être changée :
 - Project Settings -> Time -> Timescale
    - 1 pour du temps réel
    - inférieur à 1 sorte de slowmotion laissant plus de temps pour calculer chaque frame (permettant des simulations avec un nombre important de particules)

Le nombre de particules peut être paramètré dans l'inspecteur de l'objet waterGenerator
